extends RigidBody2D

onready var terrain = get_parent().get_parent()
export var size = 256
var min_size = 4
var shape
var region_origin = Vector2(0,0)
var newNode = []
var owner_id = create_shape_owner(self)

func _ready():
	shape_owner_clear_shapes(owner_id)
	var new_shape = terrain.get_node(str("Shapes/Shape",size)).get_shape()
	shape_owner_add_shape(owner_id, new_shape)
	get_node("Sprite").set_region_rect(Rect2(region_origin,Vector2(size,size)))

func _on_block_mouse_enter():
	subdivide()

func subdivide():
	if size / 2 >= min_size:
		var new_size = size / 2
		for i in range(4):
			newNode.append(duplicate())
			newNode[i].size = new_size
		
		newNode[0].set_position(get_position() + Vector2(-new_size/2, -new_size/2))
		newNode[1].set_position(get_position() + Vector2(new_size/2, -new_size/2))
		newNode[2].set_position(get_position() + Vector2(-new_size/2, new_size/2))
		newNode[3].set_position(get_position() + Vector2(new_size/2, new_size/2))
		
		newNode[0].region_origin = region_origin
		newNode[1].region_origin = region_origin + Vector2(new_size, 0)
		newNode[2].region_origin = region_origin + Vector2(0, new_size)
		newNode[3].region_origin = region_origin + Vector2(new_size, new_size)
		
		for i in range(4):
			get_parent().add_child(newNode[i])
	
	self.queue_free()

func _on_RigidBody2D_body_entered(body):
	if body.has_method('fake'):
		subdivide()
