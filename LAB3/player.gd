extends KinematicBody2D

export (int) var run_speed = 130
export (int) var jump_speed = -300
export (int) var gravity = 900

var velocity = Vector2()
var jumping = false
var Rocket = preload("res://Rocket.tscn")
var Torch = preload("res://Torch.tscn")
var Grenade = preload("res://Grenade.tscn")
var selected_wep = Rocket

func get_input():
	velocity.x = 0
	var right = Input.is_action_pressed("right")
	var left = Input.is_action_pressed("left")
	var jump = Input.is_action_just_pressed("jump")
	var fire = Input.is_action_just_pressed("destroy")
	var wep1 = Input.is_action_just_pressed("weapon1")
	var wep2 = Input.is_action_just_pressed("weapon2")
	var wep3 = Input.is_action_just_pressed("weapon3")
	
	if jump and is_on_floor():
		jumping = true
		velocity.y = jump_speed
	if jump and is_on_wall():
		jumping = true
		velocity.y = jump_speed
	if right:
		velocity.x += run_speed
		$Sprite.flip_h = true
	if left:
		velocity.x -= run_speed
		$Sprite.flip_h = false
	if fire:
		shoot()
	if wep1:
		selected_wep = Rocket
		get_parent().get_node("CanvasLayer/Popup/Label").set_text('Selected Rocket')
		get_parent().get_node("CanvasLayer/Popup").popup()
	elif wep2:
		selected_wep = Torch
		get_parent().get_node("CanvasLayer/Popup/Label").set_text('Selected Torch')
		get_parent().get_node("CanvasLayer/Popup").popup()
	elif wep3:
		selected_wep = Grenade
		get_parent().get_node("CanvasLayer/Popup/Label").set_text('Selected Grenade')
		get_parent().get_node("CanvasLayer/Popup").popup()

func shoot():
	var projectile = selected_wep.instance()
	if projectile.has_method('start'):
		projectile.start(position, get_angle_to(get_global_mouse_position()))
	get_parent().add_child(projectile)

func _physics_process(delta):
	get_input()
	velocity.y += gravity * delta
	if jumping and is_on_floor():
		jumping = false
	if is_on_wall():
		position.y -= 2
	if is_on_wall() and jumping:
		jumping = false
		position.y -= 2
	velocity = move_and_slide(velocity, Vector2(0, -1))
