extends Area2D
onready var progressbar = get_parent().get_parent().get_parent().get_node('CanvasLayer/ProgressBar')

func _ready():
	pass

func _on_Gold_body_entered(body):
	progressbar.value += 1
	queue_free()
