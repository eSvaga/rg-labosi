extends Node2D

var block_count = 0
var fps
export var min_size = 4

func _ready():
	set_physics_process(true)
	pass

func _physics_process(delta):
	block_count = get_node("Blocks").get_child_count()
	
