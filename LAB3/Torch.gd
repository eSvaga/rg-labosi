extends Area2D

func _ready():
	rotation = get_angle_to(get_global_mouse_position())
	position = get_parent().get_node('Player').position
	$Timer.set_wait_time(3)
	$Timer.set_one_shot(true)
	$Timer.start()
	set_physics_process(true)
	
func _process(delta):
	position = get_parent().get_node('Player').position
	var dir = get_global_mouse_position() - get_parent().get_node('Player').global_position
	if dir.length() > 5:
		rotation = dir.angle()

func _on_Torch_body_entered(body):
	if body.has_method('subdivide'):
		body.subdivide()

func _on_Timer_timeout():
	queue_free()
