extends Area2D

func _ready():
	$CollisionShape2D.disabled = true

func _physics_process(delta):
	position = get_global_mouse_position()
	if Input.is_action_pressed("destroy"):
		$CollisionShape2D.disabled = false
	else:
		$CollisionShape2D.disabled = true

func _on_Mouse_body_entered(body):
	if body.has_method('subdivide'):
		body.subdivide()
