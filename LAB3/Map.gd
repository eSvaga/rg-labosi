extends Node2D

var block_count = 0
var fps
export var min_size = 4
export var num_of_gold = 70
var generated_points = []
var rng = RandomNumberGenerator.new()
var Gold = preload("res://Gold.tscn")

func _ready():
	
	for i in range(num_of_gold):
		generated_points.append(Vector2(rng.randi() % 2000 + 140, rng.randi() % 1100 + 50))
	
	print('lets generate gold')
	generate_gold()
	
func generate_gold():
	for i in range(len(generated_points)):
		var new_gold = Gold.instance()
		
		new_gold.position = generated_points[i]
		
		$Gold.add_child(new_gold)	
		
func _physics_process(delta):
	block_count = get_node("Blocks").get_child_count()
	
	



