extends KinematicBody2D

export (int) var rocket_speed = 750
var velocity = Vector2()
var timer = Timer.new()
var hit = false
var gravity = 1000

func start(pos, dir):
	timer.connect("timeout", self, '_on_timeout')
	add_child(timer)
	rotation = dir
	position = pos
	velocity = Vector2(rocket_speed, 0).rotated(rotation)
	
func _physics_process(delta):
	var collision = move_and_collide(velocity * delta)
	if not collision and not hit:
		velocity.y += gravity * delta
		rotation = velocity.angle()
	if collision and not hit:
		hit = true
		$'Explosion'.set_collision_mask_bit(1, true)
		$'Explosion'.set_collision_layer_bit(0, true)
		if collision.collider.has_method('subdivide'):
			collision.collider.subdivide()
		$Sprite.visible = false
		$AnimatedSprite.visible = true	
		$AnimatedSprite.play('explosion')
		velocity = Vector2(0, 0)
		
func _on_AnimatedSprite_animation_finished():
	queue_free()

func _on_AnimatedSprite_frame_changed():
	var bodies = $Explosion.get_overlapping_bodies()
	for body in bodies:
		if body.has_method('subdivide'):
				body.subdivide()

func _on_VisibilityNotifier2D_screen_exited():
	queue_free()
