extends KinematicBody2D

export (int) var grenade_speed = 500
var velocity = Vector2()
var gravity = 10
var collision
var exploded = false

func _ready():
	var dir = get_global_mouse_position() - get_parent().get_node('Player').global_position
	rotation = dir.angle()
	position = get_parent().get_node('Player').position
	velocity = Vector2(grenade_speed, 0).rotated(rotation)
	$Timer.set_wait_time(3)
	$Timer.set_one_shot(true)
	$Timer.start()
	set_physics_process(true)
	
func _physics_process(delta):
	var collision = move_and_collide(velocity * delta)
	if collision and not exploded:
		velocity = velocity.bounce(collision.normal) / 1.5
	if not exploded:
		velocity.y += gravity
		
func _on_AnimatedSprite_animation_finished():
	queue_free()

func _on_AnimatedSprite_frame_changed():
	var bodies = $Explosion.get_overlapping_bodies()
	for body in bodies:
		if body.has_method('subdivide'):
				body.subdivide()

func _on_VisibilityNotifier2D_screen_exited():
	queue_free()

func _on_Timer_timeout():
	exploded = true
	$Sprite.visible = false
	$'Explosion'.set_collision_mask_bit(1, true)
	$'Explosion'.set_collision_layer_bit(0, true)
	$AnimatedSprite.visible = true	
	$AnimatedSprite.play('explosion')
	velocity = Vector2(0, 0)
