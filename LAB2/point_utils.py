import numpy as np

class Point:
    def __init__(self, x, y, z, h=1.0):
        self.x = float(x)
        self.y = float(y)
        self.z = float(z)
        self.h = h

    def __str__(self):
        return f'{self.x} {self.y} {self.z}'

    def normalize_then_scale(self, scale=1):
        sum = np.sqrt(self.x**2 + self.y**2 + self.z**2)
        self.x = (self.x / sum) * scale
        self.y = (self.y / sum) * scale
        self.z = (self.z / sum) * scale

