from PIL import Image
from OpenGL.GL import *
from OpenGL.GLUT import *
from OpenGL.GLU import *
import numpy as np
from random import *
import point_utils
from time import sleep

eye = point_utils.Point(40, 10, 40)

class Particle:
    def __init__(self):
        self.position = np.array([float(np.random.normal(0, 20)), 20 + randint(-2, 3), float(np.random.normal(0, 20))], dtype=np.float32)
        self.speed = np.array([0.0, 0.0, 0.0], dtype=np.float32)
        self.alive = 0
        self.size = 2
        self.color = np.array([1.0, 1.0, 1.0, 1.0], dtype=np.float32)
        self.lifetime = randint(50, 100)
        self.move_direction = np.array([0.0, -1.0, 0.0])

    def inc_alive(self):
        self.alive += 1

    def update(self):
        t = (self.lifetime - self.alive) / self.lifetime
        self.speed = self.move_direction * (9.81 * 0.1) * t * 0.7
        self.position = self.position + self.speed
        self.size = self.size
        self.color_shift(t)

    def color_shift(self, t):
        self.color[0] = t
        self.color[1] = (-1 + t**2)**2
        self.color[2] = (t)**2
        self.color[3] = t

class ParticleEmitter:
    def __init__(self, max_particles):
        self.particle_list = []
        self.max_particles = max_particles

    def create_particles(self):
        new_particles = randint(1, 3)

        for i in range(0, new_particles):
            if len(self.particle_list) < self.max_particles:
                p = Particle()
                self.particle_list.append(p)

    def increment_all(self):
        for p in self.particle_list:
            p.inc_alive()

    def update_all(self):
        remove_ind = list()
        for i in range(len(self.particle_list)):
            if self.particle_list[i].alive >= self.particle_list[i].lifetime:
                remove_ind.append(i)
            else:
                self.particle_list[i].update()

        remove_ind.reverse()
        for i in remove_ind:
            del self.particle_list[i]

def load_texture(file):
    image = Image.open(file)
    img_array = np.array(list(image.getdata()), np.uint8)
    texture = glGenTextures(1)
    gluBuild2DMipmaps(GL_TEXTURE_2D, 3, 256, 256, GL_RGB, GL_UNSIGNED_BYTE, img_array)
    glBlendFunc(GL_SRC_ALPHA, GL_ONE)
    glEnable(GL_BLEND)
    glEnable(GL_TEXTURE_2D)


def main():
    glutInit()
    glutInitDisplayMode(GLUT_RGBA | GLUT_DOUBLE | GLUT_DEPTH)
    glutInitWindowSize(1000, 1000)
    glutInitWindowPosition(200, 200)
    glutCreateWindow('Particle test')
    glMatrixMode(GL_PROJECTION)
    glLoadIdentity()
    gluPerspective(60.0, 1, 0.5, 200)
    glMatrixMode(GL_MODELVIEW)
    glEnable(GL_DEPTH_TEST)

    load_texture('snow.bmp')
    emitter = ParticleEmitter(50)


    def display():
        glClearColor(0.0, 0.0, 0.0, 1.0)
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT)

        emitter.create_particles()
        emitter.update_all()

        camera_right = np.array([eye.x, 0, 0])
        camera_up = np.array([eye.y, 0, 1.0])

        for p in emitter.particle_list:
            glBegin(GL_QUADS)
            glColor4f(p.color[0], p.color[1], p.color[2], p.color[3])
            glTexCoord2d(0, 0)
            glVertex3f(p.position[0] + (-p.size), p.position[1] + (-p.size), p.position[2])

            glTexCoord2d(1, 0)
            glVertex3f(p.position[0] + (p.size), p.position[1] + (-p.size), p.position[2])

            glTexCoord2d(1, 1)
            glVertex3f(p.position[0] + (p.size), p.position[1] + (p.size), p.position[2])

            glTexCoord2d(0, 1)
            glVertex3f(p.position[0] + (-p.size), p.position[1] + (p.size), p.position[2])
            glEnd()

        emitter.increment_all()
        glFlush()
        glutSwapBuffers()
        glutPostRedisplay()

    def reshape(width, height):
        glViewport(0, 0, width, height)
        glMatrixMode(GL_PROJECTION)
        glLoadIdentity()
        gluPerspective(90.0, 1, 0.5, 100)
        gluLookAt(eye.x, eye.y, eye.z,
                  0.0, 0.0, 0.0,
                  0.0, 1.0, 0.0)
        glMatrixMode(GL_MODELVIEW)

    def updatePerspective():
        glMatrixMode(GL_PROJECTION)
        glLoadIdentity()
        gluPerspective(90.0, 1, 0.5, 100)
        gluLookAt(eye.x, eye.y, eye.z,
                  0.0, 0.0, 0.0,
                  0.0, 1.0, 0.0)
        glMatrixMode(GL_MODELVIEW)

    def key_pressed(key, x, y):
        key_pressed = False
        if key == b'a':
            key_pressed = True
            eye.x = eye.x + 0.5
        elif key == b's':
            key_pressed = True
            eye.y = eye.y + 0.5
        elif key == b'd':
            key_pressed = True
            eye.z = eye.z + 0.5
        elif key == b'f':
            key_pressed = True
            eye.x = eye.x - 0.5
        elif key == b'g':
            key_pressed = True
            eye.y = eye.y - 0.5
        elif key == b'h':
            key_pressed = True
            eye.z = eye.z - 0.5
        elif key == b'p':
            print(eye)

        if key_pressed:
            updatePerspective()
            glutPostRedisplay()

    glutReshapeFunc(reshape)
    glutDisplayFunc(display)
    glutKeyboardFunc(key_pressed)

    glutMainLoop()


if __name__ == "__main__":
    old_time = 0
    main()