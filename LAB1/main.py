import global_vars
from draw_main import Draw
from spline import Bspline
from obj_reader import *


if __name__ == '__main__':
    Bspline.read_spline_points('spline.txt')
    Bspline.find_spline_points()
    Bspline.find_spline_tangent()
    Bspline.calculate_model_angle()
    Bspline.calculate_spline_rot()

    read_obj('tetrahedron.obj')

    renderer = Draw()
    renderer.start()

