import global_vars
from point_utils import Point


def read_obj(file):
    lines = None
    with open(file, 'r') as raw:
        lines = raw.readlines()

    for line in lines:
        line = line.strip()

        if line.startswith('v'):
            values = line[1:].split()
            vertex = Point(values[0], values[1], values[2])
            global_vars.verts.append(vertex)
        elif line.startswith('f'):
            values = line[1:].split()
            temp_val = [int(values[0]), int(values[1]), int(values[2])]
            global_vars.faces.append(temp_val)