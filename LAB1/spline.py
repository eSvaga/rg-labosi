import numpy as np
from point_utils import Point
import global_vars


class Bspline:

    @staticmethod
    def read_spline_points(file):
        lines = None
        with open(file, 'r') as raw:
            lines = raw.readlines()

        for line in lines:
            line = line.strip()

            values = line.split()
            vertex = Point(values[0], values[1], values[2])
            global_vars.spline_points.append(vertex)

    @staticmethod
    def find_spline_points():
        b_3 = np.array(([-1, 3, -3, 1],
                       [3, -6, 3, 0],
                       [-3, 0, 3, 0],
                       [1, 4, 1, 0]))

        steps = len(global_vars.spline_points) - 3

        for step in range(0, steps):
            t = 0
            incr = 0.01
            points = global_vars.spline_points
            r_mat = np.array(([points[step].x, points[step].y, points[step].z, 1],
                             [points[step+1].x, points[step+1].y, points[step+1].z, 1],
                             [points[step+2].x, points[step+2].y, points[step+2].z, 1],
                             [points[step+3].x, points[step+3].y, points[step+3].z, 1]))
            for i in range(0, 100):
                t_mat = np.array([t**3, t**2, t, 1]) * (1/6)

                p_t_temp = np.matmul(np.matmul(t_mat, b_3), r_mat)
                # print(p_t_temp)
                p_t = Point(p_t_temp[0], p_t_temp[1], p_t_temp[2])
                global_vars.calculated_spline_points.append(p_t)
                t += incr

    @staticmethod
    def find_spline_tangent():
        b_3d = np.array(([-1, 3, -3, 1],
                         [2, -4, 2, 0],
                         [-1, 0, 1, 0]))

        steps = len(global_vars.spline_points) - 3

        for step in range(0, steps):
            t = 0
            incr = 0.01
            points = global_vars.spline_points
            r_mat = np.array(([points[step].x, points[step].y, points[step].z, 1],
                             [points[step+1].x, points[step+1].y, points[step+1].z, 1],
                             [points[step+2].x, points[step+2].y, points[step+2].z, 1],
                             [points[step+3].x, points[step+3].y, points[step+3].z, 1]))
            for i in range(0, 100):
                t_mat = np.array([t**2, t, 1]) * (1/2)

                p_t_temp = np.matmul(np.matmul(t_mat, b_3d), r_mat)
                # print(p_t_temp)
                p_t = Point(p_t_temp[0], p_t_temp[1], p_t_temp[2])
                global_vars.point_tangents.append(p_t)
                t += incr

    @staticmethod
    def calculate_model_angle():
        s = np.array(([0, 0, 1]))

        for i in range(len(global_vars.point_tangents)):
            p = global_vars.point_tangents[i]
            e = np.array(([p.x, p.y, p.z]))
            axis = np.cross(s, e)
            global_vars.rot_axis.append(Point(axis[0], axis[1], axis[2]))

            angle = np.degrees(np.arccos(np.dot(s, e) / np.linalg.norm(e)))
            global_vars.angle.append(angle)

    @staticmethod
    def calculate_spline_rot():
        b_3 = np.array(([-1, 3, -3, 1],
                        [3, -6, 3, 0],
                        [-3, 0, 3, 0],
                        [1, 4, 1, 0]))

        steps = len(global_vars.spline_points) - 3

        for step in range(0, steps):
            t = 0
            incr = 0.01
            points = global_vars.spline_points
            r_mat = np.array(([points[step].x, points[step].y, points[step].z, 1],
                              [points[step + 1].x, points[step + 1].y, points[step + 1].z, 1],
                              [points[step + 2].x, points[step + 2].y, points[step + 2].z, 1],
                              [points[step + 3].x, points[step + 3].y, points[step + 3].z, 1]))
            for i in range(0, 100):
                t_mat = np.array(([6*t, 2, 0, 0]))

                p_t_temp = np.matmul(np.matmul(t_mat, b_3), r_mat)
                # print(p_t_temp)
                p_t = Point(p_t_temp[0], p_t_temp[1], p_t_temp[2])
                global_vars.spline_rot.append(p_t)
                t += incr