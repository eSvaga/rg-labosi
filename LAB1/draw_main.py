from OpenGL.GL import *
from OpenGL.GLUT import *
from OpenGL.GLU import *
import numpy as np

import global_vars
from point_utils import Point

class Draw:
    def __init__(self):
        self.step = 0
        self.max_step = len(global_vars.calculated_spline_points)

    def start(self):
        glutInitDisplayMode(GLUT_RGB | GLUT_DOUBLE | GLUT_DEPTH)
        glutInitWindowSize(1000, 1000)
        glutInitWindowPosition(200, 200)
        glutInit()
        glutCreateWindow(global_vars.wnd_name)
        glutReshapeFunc(self.reshape)
        glutDisplayFunc(self.display)
        glutKeyboardFunc(self.key_pressed)
        global_vars.eye = Point(0, 70, 50)
        glMatrixMode(GL_PROJECTION)
        glLoadIdentity()
        gluPerspective(80.0, 1000 / 1000, 0.5, 100)
        glMatrixMode(GL_MODELVIEW)

        glutTimerFunc(5, self.update, 0);
        glutMainLoop()


    def display(self):
        glClearColor(0.1, 0.39, 0.88, 1.0)
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT)
        self.draw()
        # self.draw2()
        glutSwapBuffers()

    def reshape(self, width, height):
        global_vars.wnd_width = width
        global_vars.wnd_height = height
        glViewport(0, 0, width, height)
        glEnable(GL_DEPTH_TEST)
        glMatrixMode(GL_PROJECTION)
        glLoadIdentity()
        gluPerspective(45.0, width / height, 0.5, 100)
        gluLookAt(global_vars.eye.x, global_vars.eye.y, global_vars.eye.z,
                  0.0, 0.0, 30.0,
                  0.0, 0.0, 1.0)
        glMatrixMode(GL_MODELVIEW)

    def updatePerspective(self):
        glMatrixMode(GL_PROJECTION)
        glLoadIdentity()

        gluPerspective(45.0, global_vars.wnd_width / float(global_vars.wnd_height), 0.5, 100)
        gluLookAt(global_vars.eye.x, global_vars.eye.y, global_vars.eye.z,
                  0.0, 0.0, 30.0,
                  0.0, 0.0, 1.0)
        glMatrixMode(GL_MODELVIEW)

    def draw(self):

        for i in range(0, len(global_vars.calculated_spline_points) - 1):

            glColor3f(1.0, 1.0, 0.0)
            glBegin(GL_LINES)
            p1 = global_vars.calculated_spline_points[i]
            p2 = global_vars.calculated_spline_points[i + 1]
            glVertex3f(p1.x, p1.y, p1.z)
            glVertex3f(p2.x, p2.y, p2.z)
            glEnd()

        destination = global_vars.calculated_spline_points[self.step]
        axis = global_vars.rot_axis[self.step]
        glPushMatrix()
        glTranslate(destination.x, destination.y, destination.z)
        glRotate(global_vars.angle[self.step], axis.x, axis.y, axis.z)
        glScale(7, 7, 7)
        glColor3f(1.0, 1.0, 0.0)
        for i in range(0, len(global_vars.faces)):

            face = global_vars.faces[i]
            f1 = global_vars.verts[face[0] - 1]
            f2 = global_vars.verts[face[1] - 1]
            f3 = global_vars.verts[face[2] - 1]
            glBegin(GL_TRIANGLES)
            glVertex3f(f1.x, f1.y, f1.z)
            glVertex3f(f2.x, f2.y, f2.z)
            glVertex3f(f3.x, f3.y, f3.z)
            glEnd()

        glPopMatrix()
        glColor3f(0.0, 1.0, 0.0)
        glBegin(GL_LINES)
        p1 = global_vars.calculated_spline_points[self.step]
        p_t = global_vars.point_tangents[self.step]
        p_t.normalize_then_scale(4)
        glVertex3f(p1.x, p1.y, p1.z)
        glVertex3f(p1.x + p_t.x, p1.y + p_t.y, p1.z + p_t.z)
        glEnd()

    def draw2(self):

        for i in range(0, len(global_vars.calculated_spline_points) - 1):

            glColor3f(1.0, 1.0, 0.0)
            glBegin(GL_LINES)
            p1 = global_vars.calculated_spline_points[i]
            p2 = global_vars.calculated_spline_points[i + 1]
            glVertex3f(p1.x, p1.y, p1.z)
            glVertex3f(p2.x, p2.y, p2.z)
            glEnd()

        destination = global_vars.calculated_spline_points[self.step]
        r1 = global_vars.point_tangents[self.step]
        r2 = global_vars.spline_rot[self.step]
        r1_a = np.array(([r1.x, r1.y, r1.z]))
        r2_a = np.array(([r2.x, r2.y, r2.z]))
        u = np.cross(r1_a, r2_a)
        w = r1_a
        v = np.cross(w, u)
        rot = np.array(([w[0], u[0], v[0]],
                        [w[1], u[1], v[1]],
                        [w[2], u[2], v[2]]))
        # print(rot)
        rot_inv = np.linalg.inv(rot)
        # print(rot_inv)
        glPushMatrix()
        # glTranslate(destination.x, destination.y, destination.z)
        # glScale(7, 7, 7)
        glColor3f(1.0, 1.0, 1.0)
        for i in range(0, len(global_vars.faces)):

            face = global_vars.faces[i]
            f1 = global_vars.verts[face[0] - 1]
            f2 = global_vars.verts[face[1] - 1]
            f3 = global_vars.verts[face[2] - 1]
            f1_a = rot_inv @ np.array(([f1.x, f1.y, f1.z])).transpose()
            f2_a = rot_inv @ np.array(([f2.x, f2.y, f2.z])).transpose()
            f3_a = rot_inv @ np.array(([f3.x, f3.y, f3.z])).transpose()
            # print(f1_a)
            glBegin(GL_TRIANGLES)
            glVertex3f(f1_a[0], f1_a[1], f1_a[2])
            glVertex3f(f2_a[0], f2_a[1], f2_a[2])
            glVertex3f(f3_a[0], f3_a[1], f3_a[2])
            glEnd()

        glPopMatrix()
        glColor3f(0.0, 1.0, 0.0)
        glBegin(GL_LINES)
        p1 = global_vars.calculated_spline_points[self.step]
        p_t = global_vars.point_tangents[self.step]
        p_t.normalize_then_scale(4)
        glVertex3f(p1.x, p1.y, p1.z)
        glVertex3f(p1.x + p_t.x, p1.y + p_t.y, p1.z + p_t.z)
        glEnd()

    def update(self, update):
        self.step += 1
        if self.step == self.max_step:
            self.step = 0
        glutPostRedisplay()
        glutTimerFunc(5, self.update, 0);


    def key_pressed(self, key, x, y):
        key_pressed = False
        if key == b'a':
            key_pressed = True
            global_vars.eye.x = global_vars.eye.x + 0.2
        elif key == b's':
            key_pressed = True
            global_vars.eye.y = global_vars.eye.y + 0.2
        elif key == b'd':
            key_pressed = True
            global_vars.eye.z = global_vars.eye.z + 0.2
        elif key == b'f':
            key_pressed = True
            global_vars.eye.x = global_vars.eye.x - 0.2
        elif key == b'g':
            key_pressed = True
            global_vars.eye.y = global_vars.eye.y - 0.2
        elif key == b'h':
            key_pressed = True
            global_vars.eye.z = global_vars.eye.z - 0.2
        elif key == b'p':
            print(global_vars.eye)

        if key_pressed:
            self.updatePerspective()
            glutPostRedisplay()